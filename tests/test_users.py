import pytest
import requests
import json

# Check creating a User
def test_add_user():
    data = {
        'name':'alex',
        'email':'alex@test.ru',
        'password':'123'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/users/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)

    assert response.json()['result']['name'] == 'alex' and response.json()['result']['email'] == 'alex@test.ru'

def main():
    return 1

if __name__ == "__main__":
    main()