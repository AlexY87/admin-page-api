import pytest
import requests
import json

# Check, that admin user succesfull created
def test_add_admin_from_superadmin():
    data = {
        'name':'test',
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)    
    created_admin = response.json()['result']
    assert response.status_code == 200 and created_admin['name'] == 'test' and created_admin['email'] == 'test@test.ru'
    
# Check, that we can not create an admin with the same email
def test_add_dublicate_user():
    data = {
        'name':'test',
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)    
    assert response.status_code == 400

def test_add_admin_from_admin():
    data = {
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/login/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    session_id = response.json()['result']['session_id']    

    data = {
        'name':'test2',
        'password':'123',
        'email':'test2@test.ru'
    }
    headers = {
        'Content-Type':'application/json',
        'X-session-id':session_id
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    created_admin = response.json()['result']
    assert response.status_code == 200 and created_admin['name'] == 'test2' and created_admin['email'] == 'test2@test.ru'

def test_get_user():

    # Making a login
    data = {
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/login/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    session_id = response.json()['result']['session_id']    

    # Creating a new user
    data = {
        'name':'test3',
        'password':'123',
        'email':'test3@test.ru'
    }
    headers = {
        'Content-Type':'application/json',
        'X-session-id':session_id
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    created_admin = response.json()['result']

    # Getting information about created user
    URL = 'http://127.0.0.1:5000/api/administration/users/{id}'.format(id = created_admin['id'])
    response = requests.get(URL, headers = headers)
    admin_for_check = response.json()['result']
    assert response.status_code == 200 and admin_for_check['name'] == 'test3' and admin_for_check['email'] == 'test3@test.ru'

def test_update_admin():
    # Making a login
    data = {
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/login/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    session_id = response.json()['result']['session_id'] 

    # Creating a new user
    data = {
        'name':'test4',
        'password':'123',
        'email':'test4@test.ru'
    }
    headers = {
        'Content-Type':'application/json',
        'X-session-id':session_id
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    created_admin_id = response.json()['result']['id']

    # Updating info about a user
    data = {
        'name':'updated'
    }
    headers = {
        'Content-Type':'application/json',
        'X-session-id':session_id
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/{id}'.format(id = created_admin_id)

    response = requests.put(URL, data = json.dumps(data), headers = headers)
    assert response.json()['result']['name'] == 'updated'

def test_get_admins_list():

    # Making a login
    data = {
        'password':'123',
        'email':'test@test.ru'
    }
    headers = {
        'Content-Type':'application/json'
    }
    URL = 'http://127.0.0.1:5000/api/administration/users/login/'

    response = requests.post(URL, data = json.dumps(data), headers = headers)
    session_id = response.json()['result']['session_id'] 

    URL = 'http://127.0.0.1:5000/api/administration/users/'

    response = requests.get(URL, headers = headers)
    admins_list = response.json()['result']
    admin_emails_for_check = ['test@test.ru', 'test2@test.ru', 'test3@test.ru', 'test4@test.ru']
    for admin in admins_list:
        admin_emails_for_check.pop(admin_emails_for_check.index(admin['email']))
    assert len(admin_emails_for_check) == 0

def main():
    print("test")

if __name__ == "__main__":
    main()    


