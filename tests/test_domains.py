import pytest
import requests
import json

def test_add_domain():
    # Creating a User to use it as Domain owner
    data = {
        'name':'domain owner',
        'email':'owner@test.ru',
        'password':'123'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/users/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    owner_id = response.json()['result']['id']

    # Creating a Domain
    data = {
        'name':'test domain',
        'owner_id':owner_id
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)

    assert response.json()['result']['name'] == 'test domain' and response.json()['result']['owner_id'] == owner_id

def test_block_domain():
    # Creating a User to use it as Domain owner
    data = {
        'name':'test_block',
        'email':'owner@test.ru',
        'password':'123'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/users/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    owner_id = response.json()['result']['id']

    # Creating a Domain
    data = {
        'name':'test_block_domain',
        'owner_id':owner_id
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    domain_id_for_block = response.json()['result']['id']

    # Blocking a created domain
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/block/{id}'.format(id = domain_id_for_block)
    response = requests.post(URL, auth = user)  
    domain_id_for_unblock = response.json()['result']['id']
    assert response.json()['result']['status'] == 'Blocked'

def test_unblock_domain():
    # Creating a User to use it as Domain owner
    data = {
        'name':'test_block',
        'email':'avu7891@mail.ru',
        'password':'123'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/users/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    owner_id = response.json()['result']['id']

    # Creating a Domain
    data = {
        'name':'test_block_domain',
        'owner_id':owner_id
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    domain_id_for_block = response.json()['result']['id']

    # Blocking a created domain
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/block/{id}'.format(id = domain_id_for_block)
    response = requests.post(URL, auth = user)  
    domain_id_for_unblock = response.json()['result']['id']

    # Unblocking a created domain
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/unblock/{id}'.format(id = domain_id_for_unblock)
    response = requests.post(URL, auth = user)  
    assert response.json()['result']['status'] == 'Active'

def test_get_list_domains():
    # Creating a User to use it as Domain owner
    data = {
        'name':'test_block',
        'email':'test111@test.ru',
        'password':'123'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/users/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)
    owner_id = response.json()['result']['id']

    # Creating first Domain
    data = {
        'name':'first_domain',
        'owner_id':owner_id
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)

    # Creating second Domain
    data = {
        'name':'second_domain',
        'owner_id':owner_id
    }
    headers = {
        'Content-Type': 'application/json'
    }
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'  
    response = requests.post(URL, data = json.dumps(data), headers = headers, auth = user)

    # Getting list of domains
    user = ('alex315','Siski-Piski12#')
    URL = 'http://127.0.0.1:5000/api/administration/domains/'   
    response = requests.get(URL, auth = user)
    domains_in_response = response.json()['result']
    domains_for_check = ['first_domain','second_domain']
    for domain in domains_in_response:
        domains_for_check.pop(domains_for_check.index(domain['name']))
        
    assert len(domains_for_check) == 0 

def main():
    return 1

if __name__ == "__main__":
    main()