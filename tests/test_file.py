import json

request = {
    'name':'test',
    'owner_id':12,
    'random':'1ksfnvd'
}

request_keys_list = list(request.keys())
if 'name' in request.keys():
    name = request.pop('name')
if 'owner_id' in request.keys():
    owner_id = request.pop('owner_id')

print(len(request.keys()), name, owner_id)

a = '1'
b = ['1', '2', '3', '4']

print(a in b)

class Test():
    
    def __init__(self, a, b):
        self.a = a
        self.b = b

test = Test(10,5)
print(test.__dict__)
print(test.__setattr__('a', 1))
print(test.__dict__)

class StuffHolder:
    stuff = "class stuff"

a = StuffHolder()
b = StuffHolder()
a.stuff     # "class stuff"
b.stuff     # "class stuff"

b.b_stuff = "b stuff"
b.b_stuff   # "b stuff"
# a.b_stuff   # AttributeError

print(StuffHolder.__dict__)    # {... 'stuff': 'class stuff' ...}
print(a.__dict__)              # {}
print(b.__dict__)              # {'b_stuff': 'b stuff'}