#python3 runner.py runserver -p 5000
#python3 runner.py gunicorn -b 0.0.0.0:5000

from app import app
from flask_script import Manager, Command, Option

class GunicornApp(Command):

    def get_options(self):
        from gunicorn.config import make_settings

        settings = make_settings()
        options = (
            Option(*klass.cli, dest=klass.name, default=klass.default)
            for setting, klass in settings.items() if klass.cli
        )
        return options

    def __call__(self, app=None, *args, **kwargs):

        from gunicorn.app.base import Application
        class FlaskApplication(Application):
            def init(self, parser, opts, args):
                return kwargs

            def load(self):
                return app

        FlaskApplication().run()

if __name__ == "__main__":
    manager = Manager(app)
    manager.add_command("gunicorn", GunicornApp())
    manager.run()
    