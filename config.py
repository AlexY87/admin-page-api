#Путь к базе данных
#Дефолтный логин и пароль супер администратора

import os

app_dir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig:
    SQLALCHEMY_MIGRATE_REPO = os.path.join(app_dir, 'db_repository')
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'A SECRET KEY'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SENDPULSE_CLIENT_ID = os.environ.get('SENDPULSE_CLIENT_ID') or 'A SECRET KEY'
    SENDPULSE_CLIENT_SECRET = os.environ.get('SENDPULSE_CLIENT_SECRET') or 'A SECRET KEY'

class DevelopementConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:vbvj000@127.0.0.1:5432/servio'
    SQLALCHEMY_BINDS = {
    'users': 'postgres://postgres:vbvj000@127.0.0.1:5432/servio'
    }
    DEBUG = True

class ProductionConfig(BaseConfig):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:vbvj000@postgres:5432/servio'
    SQLALCHEMY_BINDS = {
    'users': 'postgres://postgres:vbvj000@postgres:5432/servio'
    }
