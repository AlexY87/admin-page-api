from app import db

def main():
    db.drop_all()
    db.create_all()
    db.session.commit()

if __name__ == "__main__":
    main()