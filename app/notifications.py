from app import app
import json
import requests

class Notifications:
    def get_token():
        payloads = {
            "grant_type":"client_credentials",
            "client_id":app.config['SENDPULSE_CLIENT_ID'],
            "client_secret":app.config['SENDPULSE_CLIENT_SECRET']    
        }
        req_headers = {
            'Content-Type':'application/json'
        }
        response = requests.post('https://api.sendpulse.com/oauth/access_token', data=json.dumps(payloads), headers=req_headers)
        data = response.json()
        return data

    def send_greeting(name, email):
        auth = Notifications.get_token()
        letter = {
            "email": {
                "subject": "Registration letter. Servio24",
                "template": {
                    "id": 350472,
                    "variables": {
                        "name": name,
                        }
                    },
            "from": {
                "name": "Servio24 notify",
                "email": "notify@mylobix.com"
                },
            "to": [
            {
                "email": email
            }
            ]
            }
        }

        req_headers = {
            'Content-Type':'application/json',
            'Authorization':auth['token_type']+' '+auth['access_token']
        }
        response = requests.post('https://api.sendpulse.com/smtp/emails', data=json.dumps(letter), headers=req_headers)
        return response

    def send_password_reset(name, email, link):
        auth = Notifications.get_token()
        letter = {
            "email": {
                "subject": "Password reset. Servio24",
                "template": {
                    "id": 353318,
                    "variables": {
                        "name": name,
                        "link": "www.mylobix.com/reset_password/"+str(link)
                        }
                    },
            "from": {
                "name": "Servio24 notify",
                "email": "notify@mylobix.com"
                },
            "to": [
            {
                "email": email
            }
            ]
            }
        }

        req_headers = {
            'Content-Type':'application/json',
            'Authorization':auth['token_type']+' '+auth['access_token']
        }
        response = requests.post('https://api.sendpulse.com/smtp/emails', data=json.dumps(letter), headers=req_headers)
        return response

    def send_adding_in_domain(name, email, domain_name):
        auth = Notifications.get_token()
        letter = {
            "email": {
                "subject": "Вы добавлены в домен сервиса Lobix",
                "template": {
                    "id": 355530,
                    "variables": {
                        "name": name,
                        "domain_name": domain_name
                        }
                    },
            "from": {
                "name": "Уведомление из Lobix",
                "email": "notify@mylobix.com"
                },
            "to": [
            {
                "email": email
            }
            ]
            }
        }

        req_headers = {
            'Content-Type':'application/json',
            'Authorization':auth['token_type']+' '+auth['access_token']
        }
        response = requests.post('https://api.sendpulse.com/smtp/emails', data=json.dumps(letter), headers=req_headers)
        return response

def main():
    auth = Notifications.get_token()
    response = Notifications.send_greeting(auth)
    print(response.text)

if __name__ == "__main__":
    main()
    


