from app import app
from flask import jsonify
from flask import abort
from flask import request
from .model import domains as Domains
from .model import admin_users as Admin
from .model import domains_users
from .notifications import Notifications

import json

# Метод для авторизации запросов пользователей
def user_auth(user_request):
    # Проверка на то, что пользователь авторизован
    email = None
    if 'X-Admin-Session-id' in user_request.headers: #Заголовок, который используется для того, чтобы создавать новых пользователей от имени админов сервиса
        isLogin = Admin.Admin_users.check_login(user_request.headers.get('X-Admin-Session-id'))
    elif 'X-Session-id' in user_request.headers:  # Заголовок для проверки авторизации среди обычных доменных пользователей
        isLogin, email = Users.User.isLogin_user(user_request.headers.get('X-Session-id'))
    else:
        isLogin = False
    if isLogin == False:
        abort(403)
    return email

# ADMINISTRATION API

# ADMIN USERS
# Добавление админ пользователя

@app.route('/index/', methods=['GET'])
def index_return():
    return "Hello!"

@app.route('/api/administration/users/check/', methods=['POST'])
def check_user():
    """
    Checking an existing username
    Returning an ID of a user

    /api/administration/users/check/

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -d
    name(string):Username for find

    Result:
    403 - Access denied
    400 - Data with error
    404 - Not found
    200 - Ok

    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# -d '{"name":"alex"}' http://127.0.0.1:5000/api/administration/users/check/
    """

    if not request.authorization:
        user_auth(request)
        
    if not 'name' in request.json:
        return jsonify({'result':'Some data is missing'}), 400
    user_for_find = Admin.Admin_users.get_user(name=request.json['name'])
    if user_for_find == None:
        return jsonify({'result':'User is missing'}), 404
    return jsonify({'result':user_for_find.serialize}), 200

@app.route('/api/administration/users/', methods=['POST'])
def add_admin_user():
    """
    Adding an admin user

    /api/administration/users/

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -d
    name(string):Name of the admin
    password(string):Password of the user

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# -d '{"name":"john", "password":"123"}' http://127.0.0.1:5000/api/administration/users/
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-Admin-Session-id: 514aacf6-5a37-4d27-b2bf-9b0747b0ec94' -d '{"name":"john", "password":"123"}' http://127.0.0.1:5000/api/administration/users/
    """
    if not request.authorization:
        user_auth(request)
    elif not((request.authorization.username == "alex315") and (request.authorization.password == "Siski-Piski12#")):
        abort(403)

    if not request.json:
        abort(400)
    else:
        user_for_find = Admin.Admin_users.get_user(name = request.json["name"])
        if user_for_find != None:
            return jsonify({'result':'Admin-User is already exist', 'id':user_for_find.id}), 400
        Admin.Admin_users.add_user(request.json)
    return jsonify({'result':'OK'}), 200

# Удаление админ пользователя
@app.route('/api/administration/users/<int:id>', methods=['DELETE'])
def delete_admin_user(id):
    """
    Deleting an admin user

    /api/administration/users/<id>

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    Result:
    200 - OK
    404 - Not found

    curl -i -u alex315:Siski-Piski12# -X DELETE http://127.0.0.1:5000/api/administration/users/2
    curl -i -H 'X-Admin-Session-id: 514aacf6-5a37-4d27-b2bf-9b0747b0ec94' -X DELETE http://127.0.0.1:5000/api/administration/users/2
    """
    if not request.authorization:
        user_auth(request)
    elif not((request.authorization.username == "alex315") and (request.authorization.password == "Siski-Piski12#")):
        abort(403)

    user_for_find = Admin.Admin_users.get_user(id = id)
    if user_for_find == None:
        abort(404)
    Admin.Admin_users.delete_user(id)
    return jsonify({'result':'OK'}), 200

# Логин админ пользователя
@app.route('/api/administration/users/login/', methods=['POST'])
def login_admin_user():
    """
    Logging an admin user

    /api/administration/users/login/

    Reqirements:

    -H
    Content-Type: application/json

    -d
    name(string):Name of the admin
    password(string):Password of the user

    Result:
    200 - OK
    403 - Access denied
    400 - Some data mission in JSON or JSON not found 

    curl -i -X POST -H 'Content-Type: application/json' -d '{"name”:”john", "password":"123"}' http://127.0.0.1:5000/api/administration/users/login/
    """

    if not request.json:
        abort(400)
    user_for_find = Admin.Admin_users.get_user(name = request.json['name'])
    if user_for_find == None:
        abort(404)
    login_result = Admin.Admin_users.login(request.json['name'], request.json['password'])
    if login_result == 403:
        abort(403)
    return  jsonify({'result':{'session_id':str(login_result)}}), 200

# Разлогин админ пользователя
@app.route('/api/administration/users/logout/<int:id>', methods=['POST'])
def logout_admin_user(id):
    """
    Logging out an admin user

    /api/administration/users/logout/<int:id>

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID

    Result:
    200 - OK
    403 - Access denied
    400 - 'X-Session-id' not found

    curl -i -X POST -H ‘X-Admin-Session-id:a133ddc3-cf78-4d4a-b8d3-3f42e95911ea' http://127.0.0.1:5000/api/administration/users/logout/2
    """

    if not 'X-Admin-Session-id' in request.headers:
        abort(400)
    isLogin = Admin.Admin_users.check_login(request.headers.get('X-Admin-Session-id'))
    if isLogin:
        user_for_find = Admin.Admin_users.get_user(id = id)
        if user_for_find == None:
            abort(404)
        logout_result = Admin.Admin_users.logout(id)
        if logout_result != None:
            abort(400)
        return jsonify({'result':'OK'}), 200
    else:
        abort(403)

# USERS API

#Логин пользователя
@app.route('/api/users/login/', methods=['POST'])
def login_user():
    """
    Logging user

    /api/users/login/

    Reqirements:

    -H
    Content-Type: application/json

    -d
    email(string):User email
    password(srting):User password

    Result:
    200 - OK
    result:{session_id}
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -d '{"email":"2@2.ru", "password":"123"}' http://127.0.0.1:5000/api/users/login/
    """

    if not request.json or not ('email' in request.json) or not ('password' in request.json):
        abort(400)
    user_for_find = Users.User.get_user(email = request.json["email"])
    if user_for_find == None:
        abort(404)
    session_id = Users.User.login(request.json["email"], request.json["password"])
    if session_id == 403:
        abort(403)
    return jsonify({'result':{'session_id':str(session_id)}}), 200

#Разлогин пользователя
@app.route('/api/users/logout/<int:id>', methods=['POST'])
def logout_user(id):
    """
    Logging user out

    /api/users/logout/<int:id>
    id:user ID you want to logout

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    Result:
    200 - OK
    result:{session_id}
    403 - Access denied
    400 - Some data are missing(not session id in Headers)
    404 - User not found

    curl -i -X POST -H '730bd5a7-f0c9-45fd-a09f-919790138f68' http://127.0.0.1:5000/api/users/logout/1
    """

    if not 'X-Session-id' in request.headers:
        abort(400)
    user_for_find = Users.User.get_user(id = id)
    if user_for_find == None:
        abort(404)
    if user_for_find.session_id != request.headers.get('X-Session-id'):
        abort(403)
    logout_result = Users.User.logout(id)
    if logout_result != None:
        abort(400)
    return jsonify({'result':'OK'}), 200

@app.route('/api/users/', methods=['POST'])
def add_user():
    """
    Add a new user

    /api/users/

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID
    Content-Type: application/json

    -d
    name(string):Name of a user
    password(string):New user`s password
    email(string):
    role_id(int):New user role_id
    Additional fields:
    site(string):User place description

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Session-id:b2994059-c884-4a08-babe-88a9870ae21b' -H 'Content-Type: application/json' -d '{"name":"user2", "email":"2@2.ru", "password":"123", "role_id":1, "site":"test"}' http://127.0.0.1:5000/api/users/
    """

    user_auth(request)
    if not request.json:
        abort(400)
    if not ('name' in request.json) or not ('email' in request.json) or not ('password' in request.json) or not ('role_id' in request.json):
        return jsonify({'result':'Some required data is missing'}), 400
    user_for_find = Users.User.get_user(email = request.json['email'])
    if user_for_find:
        return jsonify({'result':'User has already exist'}), 400
    user = Users.User.add_user(request.json)
    Notifications.send_greeting(request.json['name'], request.json['email'])
    if request.headers.get('X-Session-id') != None:
        domain = Users.User.get_domain(X_Session_id = request.headers.get('X-Session-id'))
        domains_users.Domains_Users.add_domain_user(domain.id, user.id)
        Notifications.send_adding_in_domain(user.name, user.email, domain.name)
    return jsonify({'result':'OK'}), 200

@app.route('/api/users/', methods=['DELETE'])
@app.route('/api/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id = None):
    """
    Delete a user

    /api/users/<int:user_id>
    user_id(int):user ID you want to delete

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID
    Content-Type:application/json

    -d 
    'email':email of a user to be deleted

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X DELETE -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' http://127.0.0.1:5000/api/users/2
    """

    user_auth(request)
    if user_id != None:
        user = Users.User.delete_user(id = user_id)
    else:
        if not request.json or not 'email' in request.json:
            return jsonify({'result':'Some data is missing in JSON or JSON is absent'}), 400
        user = Users.User.delete_user(email = request.json['email'])
    if user == None:
        return jsonify({'result':'User not found'}), 404
    domains_users.Domains_Users.delete_domain_user(user.id)
    return jsonify({'result':'OK'}), 200

# Обновление информации о пользователе
@app.route('/api/users/<int:id>', methods=['PUT'])
def update_user(id):
    """
    Update a user

    /api/users/<int:id>
    id(int):user ID you want to update

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    -d
    name(string):New name of this user
    site(string):New site of this user
    role_id(int):New role ID of this user

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X PUT -H 'X-Admin-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' -H 'Content-Type: application/json' -d '{"name":"1111"}' http://127.0.0.1:5000/api/users/1
    """

    user_auth(request)
    if not request.json:
        abort(400)
    user, errors = Users.User.update_user(id, request.json)
    if user == None:
        return jsonify({'result':'User doesn\'t exist'}), 404
    else:
        return jsonify({'errors':errors}), 200

@app.route('/api/users/', methods=['GET'])
@app.route('/api/users/<int:user_id>', methods=['GET'])
def get_user(user_id = None):
    """
    Get a user

    /api/users/<int:user_id>
    user_id(int):user ID you want to get

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    -d (optional)
    'email':email of a user you want to find

    Result:
    200 - OK
    result:{user}
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X GET -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' http://127.0.0.1:5000/api/users/1
    """

    user_auth(request)
    
    if user_id != None:
        print("ID is ", user_id)
        user = Users.User.get_user(id = user_id)
    else:
        if 'email' in request.json:
            user = Users.User.get_user(email = request.json['email'])
        else:
            return jsonify({'result':'There is no Email in your JSON'}), 400
    if user == None:
        abort(404)
    else:
        return jsonify({'result':user.serialize}), 200

@app.route('/api/users/list/', methods=['GET'])
def get_list_users():
    """
    Get users list

    /api/users/

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    Result:
    200 - OK
    result:{users}
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X GET -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' http://127.0.0.1:5000/api/users/list/
    """

    user_auth(request)
    who_am_i = Users.User.who_am_i(request.headers['X-Session-id'])
    list_users = Users.User.get_list_users(who_am_i.domain[0].domain_id)
    json = []
    for item in list_users:
        json.append(Users.User.get_user(id = item.user_id).serialize)

    return jsonify({'result':json}), 200

@app.route('/api/users/block/<int:user_id>', methods=['POST'])
def block_user(user_id):
    """
    Block user

    /api/users/block/<int:user_id>
    user_id(int):User ID you want to block

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' http://127.0.0.1:5000/api/users/block/2
    """

    user_auth(request)
    user = Users.User.block_user(user_id)
    if user == None:
        abort(404)
    return jsonify({'result':'OK'}), 200

@app.route('/api/users/unblock/<int:user_id>', methods=['POST'])
def unblock_user(user_id):
    """
    Unblock user

    /api/users/unblock/<int:user_id>
    user_id(int):User ID you want to unblock

    Reqirements:
    
    -H
    X-Session-id(string):Your session ID

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' http://127.0.0.1:5000/api/users/unblock/2
    """

    user_auth(request)
    user = Users.User.unblock_user(user_id)
    if user == None:
        abort(404)
    return jsonify({'result':'OK'}), 200

# Сброс пароль
@app.route('/api/users/reset_password', methods=['POST'])
def reset_password():
    """
    Send link to reser password to user

    /api/users/reset_password

    Reqirements:
    -H
    Content-Type: application/json

    -d
    email(string):Email of the user you want to change password

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -d '{"email":"avu7891@mail.ru"}' http://127.0.0.1:5000/api/users/reset_password
    """

    if not request.json or not ('email' in request.json):
        abort(400)
    user = Users.User.get_user(email = request.json['email'])
    if user == None:
        abort(404)
    Users.User.reset_password(request.json['email'])
    Notifications.send_password_reset(user.name, user.email, user.password_reset_link)
    return jsonify({'result':'OK'}), 200

# Смена пароля
@app.route('/api/users/set_password/<string:token>', methods=['POST'])
@app.route('/api/users/set_password/', methods=['POST'])
def set_new_password(token = None):
    """
    Set a new password to user

    /api/users/set_password/<string:token>
    /api/users/set_password/
    token(string):token of a user you want to change password

    Reqirements:
    -H
    Content-Type: application/json
    X-Session-id(string):Your session ID  - Use this header if you don`t have a token

    -d
    email(string):Email of the user you want to change password
    password(string):New user password

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Session-id:c9e6c4be-9139-4513-9f9f-2826ea992956' -H 'Content-Type: application/json' -d '{"email":"avu7891@mail.ru", "password":"Santa666"}' http://127.0.0.1:5000/api/users/set_password/0739c88f-aeae-418e-81a2-8ac0f487dc83
    """

    if not 'email' in request.json:
        return jsonify({'result':'User email not found in JSON'}), 400
    if not 'password' in request.json:
        return jsonify({'result':'New password not found in JSON'}), 400
    if token == None:
        email = user_auth(request)
        result = Users.User.change_password(request.json, token = None)
        if result:
            return jsonify({'result':'OK'}), 200
        else:
            return jsonify({'result':'Something went wrong'}), 400
    else:
        result = Users.User.change_password(request.json, token)
        if result:
            return jsonify({'result':'OK'}), 200
        else:
            return jsonify({'result':'Something went wrong'}), 400

    """
    Delete selected visit

    /api/visits/<int:id>
    id(int):Visit to be deleted

    Reqirements:

    -H
    X-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    "result":{visit}
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X DELETE -u alex:123 http://127.0.0.1:5002/api/visits/4
    """

    user_auth(request)
    visit = Visits.Visit.delete_visit(visit_id)
    if visit == None:
        abort(404)
    else:
        return "Ok", 200


    """
    Cancel an application

    /api/applications/cancel/<int:id>
    id(int):Application to be calceled

    Reqirements:

    -H
    X-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Session-id:02fb4b0d-f38c-4d1d-91a7-7abccc9038b7' http://127.0.0.1:5000/api/applications/cancel/1
    """

    user_auth(request)
    user = Users.User.who_am_i(request.headers['X-Session-id'])
    application = Applications.Application.get_application(id)
    if application.status_id == "1" or application.status_id == "3":
        return jsonify({'result':'Applicaton can not be cancelled'}), 400
    parameters={
        'status_id':3
    }
    updated_application, errors = Applications.Application.update_application(id, parameters, user.domain[0].domain_id)
    return jsonify({'result':'OK'}), 200

# DOMAINS API

@app.route('/api/domains/', methods=['POST'])
def add_domain():
    """
    Add a new domain

    /api/domains/

    Reqirements:

    -H
    Content-Type: application/json
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    -d
    name(string):Name of a new domain

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -H 'X-Admin-Session-id:b2994059-c884-4a08-babe-88a9870ae21b' -d '{"name":"testdomain"}' http://127.0.0.1:5000/api/domains/
    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    if not request.json or not 'name' in request.json:
        abort(400)
    result = Domains.Domain.add_domain(request.json)
    if result == 400:
        return ({'result':'Domain with such name is already exist'}), 400
    return jsonify({'result':'OK'}), 200

@app.route('/api/domains/<int:id>', methods=['GET'])
def get_domain_using_id(id):
    """
    Get a domain information

    /api/domains/<int:id>
    id(int):ID of a domain

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X GET -H 'Content-Type: application/json' -H 'X-Admin-Session-id:341e7442-0688-4f77-b4f8-fbfed441548f' http://127.0.0.1:5000/api/domains/4
    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    domain = Domains.Domain.get_domain(id)
    if domain == None:
        abort(404)
    else:
        return jsonify({'result':domain.serialize}), 200

@app.route('/api/domain/', methods=['GET'])
def get_domain_using_name():
    """
    Get a domain information using it name

    /api/domains/

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    -d 
    name(string):name of a domain you want to change owner

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X GET -H 'Content-Type: application/json' -H 'X-Admin-Session-id:3ae13579-00f9-4b20-a5ca-4b4236613cee' -d '{"name":"test_domain"}' http://127.0.0.1:5000/api/domain/
    """
    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    if not request.json:
        abort(400)
    if not 'name' in request.json:
        abort(400)
    domain = Domains.Domain.get_domain(name = request.json['name'])
    if domain == None:
        abort(404)
    else:
        return jsonify({'result':domain.serialize}), 200    


@app.route('/api/domains/', methods=['GET'])
def get_list_domains():
    """
    Get a domains information

    /api/domains/

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X GET -H 'Content-Type: application/json' -H 'X-Admin-Session-id:341e7442-0688-4f77-b4f8-fbfed441548f' http://127.0.0.1:5000/api/domains/

    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    json = []
    list_domain = Domains.Domain.get_list_domains()
    if list_domain == None:
        return abort(404)
    for item in list_domain:
        json.append(item.serialize)
    return jsonify({'result':json}), 200

@app.route('/api/domains/block/<int:id>', methods=['POST'])
def block_domain(id):
    """
    Block selected domain

    /api/domains/block/<int:id>
    id(int):ID of a domain you want to block

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -H 'X-Admin-Session-id:341e7442-0688-4f77-b4f8-fbfed441548f' http://127.0.0.1:5000/api/domains/block/4
    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    domain = Domains.Domain.block_domain(id)
    if domain == 404:
        abort(404)
    return jsonify({'result':'OK'}), 200

@app.route('/api/domains/unblock/<int:id>', methods=['POST'])
def unblock_domain(id):
    """
    Unblock selected domain

    /api/domains/unblock/<int:id>
    id(int):ID of a domain you want to unblock

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -H 'X-Admin-Session-id:341e7442-0688-4f77-b4f8-fbfed441548f' http://127.0.0.1:5000/api/domains/unblock/4
    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    domain = Domains.Domain.unblock_domain(id)
    if domain == 404:
        abort(404)
    return jsonify({'result':'OK'}), 200

@app.route('/api/domains/change_owner/', methods=['POST'])
def change_owner():
    """
    Change owner of a domain. You need to use this method after domain creating.

    /api/domains/change_owner/

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token
    Content-Type: application/json

    -d
    id(int):ID of a domain you want to change owner
    owner_id(int):User ID of a new domain owner

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'Content-Type: application/json' -H 'X-Admin-Session-id:b2994059-c884-4a08-babe-88a9870ae21b' -d '{"id":"3", "owner_id":"1"}' http://127.0.0.1:5000/api/domains/change_owner/
    """

    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    if not request.json or not 'id' in request.json or not 'owner_id' in request.json:
        abort(400)
    result = Domains.Domain.change_owner(request.json)
    if result == 200:
        return jsonify({'result':'OK'}), 200
    if result == 404:
        return jsonify({'result':'Domain or user not found'}), 404

@app.route('/api/domains/<int:id>', methods=['DELETE'])
def delete_domain_using_id(id):
    """
    Delete selected domain

    /api/domains/delete/<int:id>

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X POST -H 'X-Admin-Session-id:b2994059-c884-4a08-babe-88a9870ae21b' http://127.0.0.1:5000/api/domains/3
    """
    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    domain = Domains.Domain.delete_domain(id = id)
    if domain == None:
        return jsonify({'result':'Domain not found'}), 404
    return jsonify({'result':domain.serialize}), 200

@app.route('/api/domains/', methods=['DELETE'])
def delete_domain_using_name():
    """
    Delete selected domain

    /api/domains/delete/

    Reqirements:

    -H
    X-Admin-Session-id(string):Your session ID  - Use this header if you don`t have a token
    'Content-Type':'application/json'

    -d 
    name:Name of the domain to delete

    Result:
    200 - OK
    403 - Access denied
    400 - Some data are missing
    404 - User not found

    curl -i -X DELETE -H 'X-Admin-Session-id:93b4f248-c84f-48b0-9a7f-9c299ad76959' -H 'Content-Type: application/json' -d '{"name":"home"}' http://127.0.0.1:5000/api/domains/
    """
    if 'X-Session-id' in request.headers:
        abort(403)
    user_auth(request)
    if not request.json:
        abort(401)
    if not 'name' in request.json:
        return jsonify({'result':'There are some mistakes in a sending data'}), 409
    domain = Domains.Domain.delete_domain(name = request.json['name'])
    if domain == None:
        return jsonify({'result':'Domain not found'}), 404
    return jsonify({'result':domain.serialize}), 200
    