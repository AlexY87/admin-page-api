from flask import jsonify
from flask import abort
from flask import request
from app import db, app
from datetime import datetime
from .admin_users import Admin_user
from .domains_users import Domains_Users

def dump_datetime(value):
#Deserialize datetime object into string form for JSON processing.
    if value is None:
        return None
    else:
        return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

statuses = {
    0:'Not activated',
    1:'Active',
    2:'Blocked'
}
# Класс работы с доменами, связанный с таблице в базе
class Domain(db.Model):

    __tablename__ = "domains"
    id = db.Column(db.Integer, primary_key = True, autoincrement = True, unique = True)
    name = db.Column(db.String, unique = True)                 # Название домена, как оно отображается
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))             # Владелец домена. Тут связь с пользователем из таблицы "users" по его id
    created_at = db.Column(db.DateTime)
    status = db.Column(db.String)
    owner = db.relationship('User')

    # Создание нового домена
    def __init__(self, name, owner_id):
        self.name = name
        self.owner_id = owner_id
        self.created_at = datetime.utcnow()
        self.status = statuses[1]
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return "Domain <'{id}' '{name}' '{owner}' '{created_at}' '{enabled}'>".format(id = self.id, name = self.name, owner = self.owner, created_at = self.created_at, enabled = self.enabled)

    def block(self):
        self.status = statuses[2]
        db.session.commit()

    def unblock(self):
        self.status = statuses[1]
        db.session.commit()

    @property
    def serialize(self):
        return {
            'id':self.id,
            'name':self.name,
            'status':self.status,
            'owner_id':self.owner_id,
            'created_at':dump_datetime(self.created_at)
        }
    
    @staticmethod
    def get_domain(id):
        domain = db.session.query(Domain).filter(Domain.id == id).first()
        return domain

    @staticmethod
    def get_list_domains():
        domains = db.session.query(Domain).all()
        return domains

@app.route('/api/administration/domains/', methods = ['POST'])
def add_domain():
    """
    Add a domain

    /api/administration/domains/
    Method POST

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'
    'X-session-id: {session_id}'

    -d
    name:Name of a domain
    owner_id:id of a owner user

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# -d '{"name":"john", "owner_id":"12"}' http://127.0.0.1:5000/api/administration/domains/
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' -d '{"name":"john", "owner_id":"12"}' http://127.0.0.1:5000/api/administration/domains/    
    """
    is_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not is_authorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    try:
        name = request.json['name']
        owner_id = request.json['owner_id']
    except:
        return jsonify({'result':'Bad data', 'message':'Some required data is missed in request'}), 400
    domain = Domain(name = name, owner_id = owner_id)
    domain_user = Domains_Users(user_id = owner_id, domain_id = domain_id)
    return jsonify({'result':domain.serialize, 'message':'Domain is successfull created'}), 200

@app.route('/api/administration/domains/block/<int:id>', methods = ['POST'])
def block_domain(id):
    """
    Block a domain

    /api/administration/domains/block/{id}
    Method POST

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'
    'X-session-id: {session_id}'

    Result:
    403 - Access denied
    200 - OK
    404 - Not found

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# http://127.0.0.1:5000/api/administration/domains/block/{id}
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' http://127.0.0.1:5000/api/administration/domains/block/{id}    
    """    
    is_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not is_authorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    domain = Domain.get_domain(id = id)
    if domain == None:
        return jsonify({'result':'Not found', 'message':'There is no domain with such ID'}), 404
    domain.block()

    return jsonify({'result':domain.serialize, 'message':'Domain was successfull blocked'}), 200

@app.route('/api/administration/domains/unblock/<int:id>', methods = ['POST'])
def unblock_domain(id):
    """
    Unblock a domain

    /api/administration/domains/unblock/{id}
    Method POST

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'
    'X-session-id: {session_id}'

    Result:
    403 - Access denied
    200 - OK
    404 - Not found

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# http://127.0.0.1:5000/api/administration/domains/unblock/{id}
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' http://127.0.0.1:5000/api/administration/domains/unblock/{id}    
    """    
    is_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not is_authorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    domain = Domain.get_domain(id = id)
    if domain == None:
        return jsonify({'result':'Not found', 'message':'There is no domain with such ID'}), 404
    domain.unblock()

    return jsonify({'result':domain.serialize, 'message':'Domain was successfull blocked'}), 200

@app.route('/api/administration/domains/', methods = ['GET'])
def get_domain():
    """
    Domains list

    /api/administration/domains/
    Method GET

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'
    'X-session-id: {session_id}'

    Result:
    403 - Access denied
    200 - OK
    404 - Not found

    Examples:
    curl -i -X GET -H 'Content-Type: application/json' -u alex315:Siski-Piski12# http://127.0.0.1:5000/api/administration/domains/
    curl -i -X GET -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' http://127.0.0.1:5000/api/administration/domains/    
    """
    is_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id')) 
    if not is_authorized:
        return jsonify({'return':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    domains = Domain.get_list_domains()
    if domains == None:
        return jsonify({'result':'Not found', 'message':'There are no domains'}), 404
    domains_list_json = []
    for domain in domains:
        domains_list_json.append(domain.serialize)
    return jsonify({'result':domains_list_json, 'message':'List of the domains'}), 200

@app.route('/api/administration/domains/<int:id>', methods = ['PUT'])
def update_domain(id):
    is_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session_id'))
    if not is_authorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    request_data = request.json()
    try:
        request_data.pop('name')
        request_data.pop('owner_id')
    except:
        print("Problems")
    domain = Domain.get_domain(id)
    # domain.update(name = new_domain_name, owner_id = new_domain_owner_id)
    return jsonify({'result':domain.serialize, 'message':'Domain update successfull'}), 200


@app.route('/test/', methods = ['GET'])
def test_func():
    return "test"

def main():
    Domain.add_domain("Test1", 12345)
    Domain.get_list_domains()
    Domain.get_domain(1)
    Domain.update_domain(1, {"name":"Test5"})
    Domain.block_domain(1)
    Domain.unblock_domain(1)
    

if __name__ == "__main__":
    main()

