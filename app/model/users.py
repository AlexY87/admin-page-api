from flask import jsonify
from flask import abort
from flask import request
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from .domains_users import Domains_Users
from .domains import Domain
from app import db, app
from .admin_users import Admin_user
from ..notifications import Notifications

def dump_datetime(value):
    #Deserialize datetime object into string form for JSON processing.
    if value is None:
        return None
    return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

# Класс для работы с таблицей в базе с пользователями

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key = True, autoincrement=True, unique = True)
    name = db.Column(db.String)                                   # Имя пользователя, как оно будет отображаться
    email = db.Column(db.String)                                  # Адрес электронной почты пользователя и одновременно логин пользваотеля
    password_hash = db.Column(db.String)                          # Хэш пароля
    password_reset_link = db.Column(db.String)                    # Токен для сброса пароля
    created_at = db.Column(db.DateTime)
    enabled = db.Column(db.Boolean)
    session_id = db.Column(db.String)  
    domain = db.relationship("Domains_Users", backref="users")         
   

    # Конструктор класса
    def __init__(self, name, email, password):
        self.name = name                  
        self.email = email                  
        self.password_hash = generate_password_hash(password)      
        self.password_reset_link = None                    
        self.created_at = datetime.utcnow()
        self.enabled = True
        self.session_id = None
        db.session.add(self)
        db.session.commit()


    def __repr__(self):
        return "User <'{id}' '{name}' '{email}' '{enabled}' '{created_at}' '{session_id}'>".format(id = self.id, name = self.name, email = self.email, enabled = self.enabled, created_at = self.created_at, session_id = self.session_id)
    
    @property
    def serialize(self):
        #Return object data in easily serializable format
        return {
            'id':self.id,
            'name':self.name,
            'password_hash':self.password_hash,
            'created_at':dump_datetime(self.created_at),
            'enabled':self.enabled,
            'email':self.email
        }

@app.route('/api/users/', methods = ['POST'])
def add_user():
    """
    Add a user

    /api/users/
    Method POST

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'
    'X-session-id':{sesion id of an admin user}

    -d
    name(string):Name of a user
    password(string):Password of a user
    email:Email of a user

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# -d '{"name":"john", "password":"123", "email":"avu789233333321@gmail.com"}' http://127.0.0.1:5000/api/users/
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' -d '{"name":"john", "password":"123"}' http://127.0.0.1:5000/api/users/    
    """
    is_admin_authorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not is_admin_authorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    try:
        name = request.json['name']
        email = request.json['email']
        password = request.json['password'] 
    except:
        return jsonify({'result':'Bad data', 'message':'Some data are missing or wrong'}), 400
    user = User(name = name, email = email, password = password)
    Notifications.send_greeting(name = user.name, email = user.email)
    return jsonify({'result':user.serialize}), 200

def main():
    print("Good!")

if __name__ == "__main__":
    main()