from flask import jsonify
from flask import abort
from flask import request
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
import uuid
from app import db, app

def dump_datetime(value):
#Deserialize datetime object into string form for JSON processing.
    if value is None:
        return None
    else:
        return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

#Класс для работы с пользователями административной части
class Admin_user(db.Model):

    __tablename__ = "admin_users"
    id = db.Column(db.Integer, primary_key = True, autoincrement = True, unique = True)
    name = db.Column(db.String)
    email = db.Column(db.String)
    password_hash = db.Column(db.String)
    session_id = db.Column(db.String)
    created_at = db.Column(db.DateTime)

    def __init__(self, name, email, password):
        self.name = name
        self.password_hash = generate_password_hash(password)
        self.email = email
        self.session_id = None
        self.created_at = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return "User <{id}, {name}, email - {email}, session_id - {session_id}, created_at - {created_at}>".format(id = self.id, email = self.email, name = self.name, session_id = self.session_id, created_at = self.created_at)

    def login(self):
        self.session_id = uuid.uuid4()
        db.session.commit()

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,            
            'email': self.email,
            'created_at':dump_datetime(self.created_at)
        }

    def update(self, name = None):
        self.name = name
        db.session.commit()

    @staticmethod
    def get_admin(email = None, id = None):
        if id == None and email != None:
            user = db.session.query(Admin_user).filter(Admin_user.email == email).first()
        elif id != None and email == None:
            user = db.session.query(Admin_user).filter(Admin_user.id == id).first()
        elif id == None and email == None:
            user = db.session.query(Admin_user).all()
        return user

    @staticmethod
    def check_admin_login(authorization, x_session_id):
        """
        Return False or True if user authorized
        """
        if authorization != None:
            isAuthorized = authorization.username == 'alex315' and authorization.password == 'Siski-Piski12#'
        else:
            isAuthorized = db.session.query(Admin_user).filter(Admin_user.session_id == x_session_id).first() != None
        return isAuthorized

@app.route('/api/administration/users/', methods = ['POST'])
def add_admin_user():
    """
    Add an admin user

    /api/administration/users/
    Method POST

    Reqirements:

    -u 
    alex315:Siski-Piski12#

    -H 
    'Content-Type: application/json'

    -d
    name(string):Name of the admin
    password(string):Password of the user
    email:Email of an admin user

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -u alex315:Siski-Piski12# -d '{"name":"john", "password":"123", "email":"avu789233333321@gmail.com"}' http://127.0.0.1:5000/api/administration/users/
    curl -i -X POST -H 'Content-Type: application/json' -H 'X-session-id: af140594-bc2e-458b-9793-86df7e9f3dcd' -d '{"name":"john", "password":"123"}' http://127.0.0.1:5000/api/administration/users/    
    """

    isAuthorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not isAuthorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    
    try:
        name = request.json['name']
        email = request.json['email']
        password = request.json['password']
    except:
        return jsonify({'result':'Bad data', 'message':'Some required data is missed in request'}), 400

    user = Admin_user.get_admin(email = email)
    if user != None:
        return jsonify({'result':'Bad data', 'message':'User with such email is already exist'}), 400

    user = Admin_user(name = name, email = email, password = password)
    return jsonify({'result':user.serialize, 'message':'Admin user succesfull created'}), 200

@app.route('/api/administration/users/login/', methods = ['POST'])
def login_admin():
    """
    Login as administrator

    /api/administration/users/
    Method POST

    -d 
    email: Email of a user you want to login
    password: Password to check

    -H 
    Content-Type: application/json

    Result:
    200 - Login succesfull
    403 - Forbidden

    Examples:
    curl -i -X POST -H 'Content-Type: application/json' -d '{"password":"123", "email":"avu789233333321@gmail.com"}' http://127.0.0.1:5000/api/administration/users/login/
    """
    try:
        email = request.json['email']
        password = request.json['password']
    except:
        return jsonify({'result':'Bad data', 'message':'Some required data is missed in request'}), 400 
    user = Admin_user.get_admin(email = email)
    if user != None:
        isValid_password = check_password_hash(user.password_hash, password)
        if isValid_password:
            user.login()
        else:
            return jsonify ({'result':'Forbidden', 'message':'Wrong password'}), 403
    else:
        return jsonify ({'result':'Forbidden', 'message':'User not found'}), 403
    return jsonify({'result':{'session_id':user.session_id}, 'message':'You have sucessfull login'}), 200

@app.route('/api/administration/users/<int:id>', methods = ['GET'])
def get_admin(id):
    """
    Get an admin user information

    /api/administration/users/{id}
    Method GET

    Reqirements:

    -u
    alex315:Siski-Piski12#

    -H 
    'X-session-id: {session_id}' 

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X GET -H 'X-session-id: a15f6259-a81c-48ea-a4e1-cdb27a1ed25e' http://127.0.0.1:5000/api/administration/users/1    
    """
    isAuthorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not isAuthorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403

    user = Admin_user.get_admin(id = id)
    if user == None:
        return jsonify ({'result':'Not found', 'message':'Admin user with such ID not found'}), 404   

    return jsonify ({'result':user.serialize, 'message':'Admin user found'}), 200

@app.route('/api/administration/users/', methods = ['GET'])
def get_admin_list():
    """
    Get an admin users list

    /api/administration/users/
    Method GET

    Reqirements:

    -u
    alex315:Siski-Piski12#

    -H 
    'X-session-id: {session_id}' 

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X GET -H 'X-session-id: a15f6259-a81c-48ea-a4e1-cdb27a1ed25e' http://127.0.0.1:5000/api/administration/users/    
    """
    isAuthorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not isAuthorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403

    users = Admin_user.get_admin()
    if users == None:
        return jsonify ({'result':'Not found', 'message':'Admin user with such ID not found'}), 404
    else:
        list_users_json = []
        for user in users:
            list_users_json.append(user.serialize)   

    return jsonify ({'result':list_users_json, 'message':'List of admin users'}), 200

@app.route('/api/administration/users/<int:id>', methods = ['PUT'])
def update_admin(id):
    """
    Update an admin users list

    /api/administration/users/
    Method PUT

    Reqirements:

    -u
    alex315:Siski-Piski12#

    -H 
    'X-session-id: {session_id}' 

    -d
    name: New username

    Result:
    403 - Access denied
    200 - OK
    400 - Data with error

    Examples:
    curl -i -X PUT -u alex315:Siski-Piski12# -H 'Content-Type: application/json' -d '{"name":"updated", "email":"123"}' http://127.0.0.1:5000/api/administration/users/36    
    """
    isAuthorized = Admin_user.check_admin_login(authorization = request.authorization, x_session_id = request.headers.get('X-session-id'))
    if not isAuthorized:
        return jsonify({'result':'Forbidden', 'message':'You are not authorized to do this action'}), 403
    print(request.values)
    try:
        new_name = request.json['name']
    except:
        return jsonify({'result':'Bad data', 'message':'Some required data is missed in request'}), 400
    user = Admin_user.get_admin(id = id)
    user.update(name = new_name)
    return jsonify({'result':user.serialize, 'message':'Admin successfull updated'}), 200

def main():
    print("module")
    return 1

if __name__ == "__main__":
    main()