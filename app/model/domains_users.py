from datetime import datetime
from app import db

class Domains_Users(db.Model):
    __tablename__ = 'domains_users'
    id = db.Column(db.Integer, primary_key = True, autoincrement = True, unique = True)
    domain_id = db.Column(db.Integer, db.ForeignKey('domains.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init(self, user_id, domain_id):
        self.domain_id = domain_id
        self.user_id = user_id
        db.session.commit()
