from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import config

app = Flask(__name__)
app.config.from_object('config.ProductionConfig')
db = SQLAlchemy(app)

# from . import routes
from .model import domains
from .model import admin_users
from .model import users



